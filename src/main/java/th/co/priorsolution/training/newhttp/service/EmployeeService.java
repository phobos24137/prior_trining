package th.co.priorsolution.training.newhttp.service;

import org.springframework.stereotype.Service;
import th.co.priorsolution.training.newhttp.component.EmployeeTransformComponent;
import th.co.priorsolution.training.newhttp.entity.jpa.EmployeesEntity;
import th.co.priorsolution.training.newhttp.model.EmployeeCriteriaModel;
import th.co.priorsolution.training.newhttp.model.EmployeeModel;
import th.co.priorsolution.training.newhttp.model.ResponseModel;
import th.co.priorsolution.training.newhttp.repository.EmployeesNativeRepository;
import th.co.priorsolution.training.newhttp.repository.EmployeesRespository;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {
    private EmployeesRespository employeesRespository;
    private EmployeesNativeRepository employeeNativeRepository;
    private EmployeeTransformComponent employeeTransformComponent;
    public EmployeeService(EmployeesRespository employeesRespository, EmployeesNativeRepository employeeNativeRepository, EmployeeTransformComponent employeeTransformComponent) {
        this.employeesRespository = employeesRespository;
        this.employeeNativeRepository = employeeNativeRepository;
        this.employeeTransformComponent = employeeTransformComponent;
    }

    public ResponseModel<List<EmployeeModel>> getEmployeeByEmployee(EmployeeCriteriaModel employeeModel){
        ResponseModel<List<EmployeeModel>> result = new ResponseModel<>();

        result.setStatus(200);
        result.setDescription("ok");
        try {
            // do some business
            List<EmployeeModel> transformedData = this.employeeNativeRepository.findEmployeeByEmployee(employeeModel);
            result.setData(transformedData);
        } catch (Exception e){
            result.setStatus(500);
            result.setDescription(e.getMessage());
        }
        return result;
    }
    public ResponseModel<List<EmployeeModel>> getEmployeeByLastNameThenResponse(String lastName){
        ResponseModel<List<EmployeeModel>> result = new ResponseModel<>();

        result.setStatus(200);
        result.setDescription("OK");
        try{
            List<EmployeesEntity> data = this.getEmployeeByLastName(lastName);
            List<EmployeeModel> transformedData = this.employeeTransformComponent.transfromEntityListToModelList(data);
            result.setData(transformedData);
        }catch (Exception e){
            result.setStatus(500);
            result.setDescription(e.getMessage());
        }


        return result;
    }

    private List<EmployeesEntity> getEmployeeByLastName(String lastName) {
        return this.employeesRespository.findByLastName(lastName);
    }

    public ResponseModel<Void> insertAndUpdateEmployee(EmployeeModel employeeModel){
        ResponseModel<Void> result = new ResponseModel<>();

        result.setStatus(201);
        result.setDescription("OK");
        try{
            EmployeesEntity employeesEntity = this.employeeTransformComponent.transfromModelToEntity(employeeModel);
            this.employeesRespository.save(employeesEntity);

        }catch (Exception e){
            result.setStatus(500);
            result.setDescription(e.getMessage());
        }


        return result;
    }
    public ResponseModel<Void> updateEmployee(EmployeeModel employeeModel){
        ResponseModel<Void> result = new ResponseModel<>();

        result.setStatus(201);
        result.setDescription("OK");
        try{
            Optional<EmployeesEntity> employeesEntityOptional = this.employeesRespository.findById(employeeModel.getEmpNo());
            if(employeesEntityOptional.isPresent()){
                EmployeesEntity employeesEntity = employeesEntityOptional.get();
                this.employeeTransformComponent.transfromModelToEntityForUpdate(employeesEntity,employeeModel);
                this.employeesRespository.save(employeesEntity);
            }else{
                result.setStatus(404);
                result.setDescription("data to update notfound");
            }


        }catch (Exception e){
            result.setStatus(500);
            result.setDescription(e.getMessage());
        }


        return result;
    }
    public ResponseModel<Void> deleteEmployee(EmployeeModel employeeModel){
        ResponseModel<Void> result = new ResponseModel<>();

        result.setStatus(201);
        result.setDescription("OK");
        try{
            EmployeesEntity employeesEntity = new EmployeesEntity();
            this.employeeTransformComponent.transfromModelToEntityForUpdate(employeesEntity,employeeModel);
            this.employeesRespository.delete(employeesEntity);

        }catch (Exception e){
            result.setStatus(500);
            result.setDescription(e.getMessage());
        }


        return result;
    }
    public ResponseModel<Integer> insertEmployeeByNativeSql(List<EmployeeModel> employeeModels){
        ResponseModel<Integer> result = new ResponseModel<>();

        result.setStatus(201);
        result.setDescription("ok");
        try {
            // do some business
            int insertedRows = this.employeeNativeRepository.insertEmployee(employeeModels);
            result.setData(insertedRows);
        } catch (Exception e){
            result.setStatus(500);
            result.setDescription(e.getMessage());
        }
        return result;
    }
}
