package th.co.priorsolution.training.newhttp.service;

import org.springframework.stereotype.Service;
import th.co.priorsolution.training.newhttp.model.AddressModel;
import th.co.priorsolution.training.newhttp.model.PersonModel;
import th.co.priorsolution.training.newhttp.model.ResponseModel;

@Service
public class AppService {


    public ResponseModel<PersonModel> getPersonDataAndResponse(){
        ResponseModel<PersonModel> result = new ResponseModel<>();

        result.setStatus(200);
        result.setDescription("OK");
        try{

            result.setData(this.getPersonData());
        }catch (Exception e){
            result.setStatus(500);
            result.setDescription(e.getMessage());
        }


        return result;
    }
    private PersonModel getPersonData(){
        PersonModel personModel = new PersonModel();
        personModel.setNickName("petch");
        personModel.setAge(26);
        return personModel;
    }

    public ResponseModel<AddressModel> getAddressDataAndResponse(){
        ResponseModel<AddressModel> result = new ResponseModel<>();

        result.setStatus(200);
        result.setDescription("OK");
        try{
            AddressModel addressModel = this.getAddressData();
            result.setData(addressModel);
        }catch (Exception e){
            result.setStatus(500);
            result.setDescription(e.getMessage());
        }

        return result;
    }
    private AddressModel getAddressData(){
        AddressModel addressModel = new AddressModel();
        addressModel.setDistrict("Bangkhuntian");
        addressModel.setSubDistrict("samdam");
        addressModel.setProvince("Bangkok");
        addressModel.setPostalCode("10150");
        addressModel.setHouseNo("91/34");

        return addressModel;
    }
}
